from django.contrib import admin
from .models import ExpenseCategory, Account, Receipt
# Register your models here.
admin.site.register(ExpenseCategory)
admin.site.register(Account)
admin.site.register(Receipt)

class ExpenseCategory(admin.ModelAdmin):
    list_dislplay = [
        "name",
        "owner",
    ]
class Account(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
        "owner",
    ]

class Receipt(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser"
    ]
