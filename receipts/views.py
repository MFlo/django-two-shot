from django.db.models import Count
from django.shortcuts import render, redirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from django.views.generic.edit import CreateView
from .models import Receipt, ExpenseCategory, Account
from .forms import ReceiptForm, ExpenseCategoryForm
# Create your views here.

# create a **class view** or **function view** for Receipt model
class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = 'receipts/account_list.html'
    context_object_name = 'accounts'

    def get_quesryset(self):
        return Account.objects.filter(User=self.request.user).annotate(
            receipts_count=Count('receipt')
        )

class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = 'receipts/category_list.html'
    context_object_name = 'categories'

    def get_queryset(self):
        return ExpenseCategory.objects.filter(User=self.request.user).annotate(
            receipts_count=Count('receipt')
        )
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = 'receipts/receipt_list.html'
    context_object_name = 'receipts'

def get_queryset(self):
    return Receipt.objects.filter(User=self.request.user)

def create_receipt(request):
    if request.method == 'POST':
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(commit=False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('receipt_list')
    else:
        form = ReceiptForm()
    return render(request, 'receipts/create_receipt.html', {'form': form})
class CreateCategoryView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    form_class = ExpenseCategoryForm
    template_name = 'receipts/create_category.html'

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

    def get_success_url(self):
        return reverse('category_list')
