from django.urls import path
from .views import create_receipt, ExpenseCategoryListView, ReceiptListView, AccountListView, CreateCategoryView

urlpatterns = [
    # path('creat/', CreateReceiptView.as_view(), name='create_receipt'),
    path("", ReceiptListView.as_view(), name="receipt_list"),
    path('create/', create_receipt, name='create_receipt'),
    path('categories/', ExpenseCategoryListView.as_view(), name='category_list'),
    path('accounts/', AccountListView.as_view(), name='account_list'),
    path('categories/create/', CreateCategoryView.as_view(), name='create_category'),
]
