from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class LoginForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150, widget=forms.PasswordInput)

class SignUpForm(UserCreationForm):
    username = forms.CharField(max_length=150, required=True)
    password1 = forms.CharField(max_length=150, widget=forms.PasswordInput, required=True)
    password2 = forms.CharField(max_length=150, widget=forms.PasswordInput, required=True)

class Meta:
    model = User
    fields = ('username', 'password1', 'password2')
