from django.urls import path
from receipts.views import ReceiptListView
from . import views

urlpatterns = [
    path('signup/', views.signup, name='signup'),
    path('logout/', views.logout_view, name='logout'),
    path('login/', views.login_view, name='login'),
    path("", ReceiptListView.as_view(), name="receipt_list"),
]
